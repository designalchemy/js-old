// vars and functions before document ready

var winWidth = $(window).width();
var windowHeight = $(window).height();
var bodyHeight = $('body').outerHeight();
var smallBrakePoint = 720; // first brake point size
var largeBrakePoint = 900;
var feesSlider;
var preventClick = true;
var FBinit = false;
var mqDesktop;
var lgDesktop;
var scrollPos = $(window).scrollTop();
var dataVal;
var productData = [];
var products;
var sliderValue = '0';
var moveValue = '0';
var currentSelectedProduct;

// Split all the selectors that are used multiple times into vars so we are only searching for them once, reducing resources

var $html = $('html');
var $footerBar = $('.floatingBar');
var footerPos = $('footer').offset().top;
var $sticky = $('.allowSticky');
var stickyParentWidth = $sticky.parent().width();
var isLegacyProduct = true;
var productList = {};

//
// Functions
//

if (!Date.now) {
    Date.now = function () {
        return new Date().getTime();
    };
}

// on slide down set o3verflow to visible

function transEnd(el, fired) {

    el.css('overflow', 'hidden');

    if (fired === 1) {

        el.on('transitionend webkitTransitionEnd', function (e) {
            el.css('overflow', 'visible');
        });

    } else {

        el.off('transitionend webkitTransitionEnd');

    }
}

// facebook login

function facebookInit() {

    if (!FBinit) {

        window.fbAsyncInit = function () {

            FB.init({
                appId: '808406955861769',
                xfbml: true,
                oauth: true,
                version: 'v2.2',
            });

            FBinit = true;

        };

        (function (d, s, id) {
            var js;
            var fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}

            js = d.createElement(s); js.id = id;
            js.src = '//connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));

    } else {

        facebookLogin();

    }

}

function facebookLogin() {

    FB.login(function (response) {

        if (response.authResponse) {

            //getting info

            FB.api('/me', function (response, authResponse) {

                //logged in
                accessToken = FB.getAuthResponse().ACCESS_TOKEN;
                window.location.href = '/dashboard/profile/facebookPhoto';

            });

        } else {

            $('.errorMessage').remove();
            $('.errorContainer').append('<div class=\"errorMessage\">Please allows us permission to access your Facebook photos, if you receive no message, ensure your browser isn\'t blocking pop up.<\/div>');
            $('.mdModal').remove();
        }

    }, { scope: 'user_photos' });

}

//image upload

function readURL(input, output, progressEl) {
    var total = input.files.length;
    var loaded = 0;

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onprogress = function (e) {

            if (e.lengthComputable) {
                var progress = parseInt(((e.loaded / e.total) * 100), 10);

                progressEl.css('width', progress + '%');
            }

        };

        reader.onload = function (e) {

            convertImgToBase64URL(e.target.result, function (base64Img) {

                $.sessionStorage('image', base64Img);

                if (output.indexOf('/') === 0) {

                    window.location.href = output;

                } else if (output.indexOf('#') === 0) {

                    window.location.hash = output;

                }

                // loaded++;

                // if (loaded == total) {
                //     $holder.addClass('finished');
                // }

            });

        };

        reader.readAsDataURL(input.files[0]);
    }

}

// convert image to 64

function convertImgToBase64URL(url, callback, outputFormat) {

    var canvas = document.createElement('CANVAS');
    var ctx = canvas.getContext('2d');
    var img = new Image();

    img.crossOrigin = 'Anonymous';

    img.onload = function () {
        var dataURL;
        var maxLongest = 1400;
        var imageRatio = img.width / img.height;

        if (img.width > img.height && img.width > maxLongest) {

            canvas.width = maxLongest;
            canvas.height = canvas.width / imageRatio;

        } else if (img.height > maxLongest) {

            canvas.height = maxLongest;
            canvas.width = canvas.height * imageRatio;

        } else {
            canvas.height = img.height;
            canvas.width = img.width;
        }

        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
        canvas = null;
    };

    img.src = url;

}

// makes a true / false result if mobile vs desk top

function makeMq() {

    if (winWidth > smallBrakePoint) {

        mqDesktop = true;

    } else {

        mqDesktop = false;

    }

    if (winWidth > largeBrakePoint) {

        lgDesktop = true;

    } else {

        lgDesktop = false;

    }
}

// change color up or down opacity

function ColorLuminance(hex, lum) {

    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }

    lum = lum || 0;

    var rgb = '#';
    var c;
    var i;

    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i * 2, 2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ('00' + c).substr(c.length);
    }

    return rgb;
}

// convert hex to rgb

function convertHex(hex, opacity) {

    hex = hex.replace('#', '');
    r = parseInt(hex.substring(0, 2), 16);
    g = parseInt(hex.substring(2, 4), 16);
    b = parseInt(hex.substring(4, 6), 16);

    result = 'rgba(' + r + ',' + g + ',' + b + ','  + opacity / 100 + ')';

    return result;
}

// Pin reveal slider function

function pinReveal(e) {

    var $this = e;
    var thisValue = $this.val();
    var percentage = 12.5;
    var finalValue = ((thisValue * percentagee) * 2) - percentage;

    $this.parent().find('.pinFill').css('width', finalValue + '%');

    $('.pinFocus').removeClass('pinFocus');

    $this.parent().find('.pinNumber:nth-child(' + thisValue + ')').addClass('pinFocus');

}

// find the closest number
function getClosest(array, num) {

    var i = 0;
    var minDiff = 1000;
    var ans;

    for (i in array) {
        var m = Math.abs(num - array[i]);
        if (m < minDiff) {
            minDiff = m;
            ans = array[i];
        }
    }

    return ans;
}

function headerFooterOporations() {

    footerPos = $('footer').offset().top;

    // sticky if not at the footer

    if ((scrollPos - ($footerBar.height() * -1)) + windowHeight > footerPos) {

        $footerBar.addClass('normalPos').css('bottom', '0');
    } else {
        $footerBar.removeClass('normalPos');

        // hidden to start with
        if (scrollPos < 100) {

            $footerBar.css('bottom', ($footerBar.height() * -1) + 'px');

        } else {

            $footerBar.css('bottom', '0');

        }
    }

    // homepage nav appear

    if ($('.index header nav') && scrollPos > 20) {

        $('header').addClass('shadow');

    } else {

        $('header').removeClass('shadow');
    }
}

function stickyCheck() {

    if ($('.allowSticky').length) {

        makeMq();
        stickyParentWidth = $sticky.parent().width();
        $sticky.width(stickyParentWidth);

        if (lgDesktop && windowHeight >= $sticky.height() + ($('header').height() + $footerBar.height())) {

            $sticky.css({
                    top: 'auto',
                    bottom: 'auto',
                    position: 'relative',
                });

            var stickyTop = $sticky.offset().top - 150;
            var top = $(this).scrollTop();
            var bottom = $('footer').offset().top;
            var bodyHeight = $('.topMargin').height() - $footerBar.height() - $('header').height();

            if (top >= stickyTop) {
                $sticky.css({
                    top: '150px',
                    bottom: 'auto',
                    position: 'fixed',
                });
            }

            if (scrollPos >= bodyHeight - $footerBar.height()) {
                $sticky.css({
                    top: 'auto',
                    bottom: 677,
                    position: 'absolute',
                });

            }

        } else {
            $sticky.css({
                width: '100%',
                position: 'relative',
                top: 'auto',
                bottom: 'auto',
            });

        }

    }

}

// on page load load correct tab + hide others

function loadCorrectTab() {

    var hash = window.location.hash.substring(1);

    if (window.location.hash && $('a[href="#' + hash + '"]').length > 0) {

        $('.activeTabButton').removeClass('activeTabButton');
        $('.tabCell').hide();

        $('#' + hash).show();
        $('a[href="#' + hash + '"]').addClass('activeTabButton');

    } else {

        $('.tabButtons a').eq(0).addClass('activeTabButton');
        $('.tabContent .tabCell').eq(0).show();

    }

}

//set a no expire cookie

function makeLongCookie(cookieName, cookieValue) {

    document.cookie = cookieName + '=' + cookieValue + '; expires=Mon, 11 Nov 2777 11:11:00 GMT; path=/';

}

// This function needs replacing at some point!!!!

function productType(productId) {
    var productName;

    switch (productId) {
        case 15:
        case 19:
            productName = 'Ffrees Zero';
            break;
        case 20:
            productName = 'Ffrees 2.5';
            break;
        case 16:
        case 21:
            productName = 'Ffrees 5 - Free ATMs';
            break;
        case 17:
        case 22:
            productName = 'Ffrees 5 - Free BACs';
            break;
        case 23:
            productName = 'Ffrees 7.5 - Free ATMs';
            break;
        case 24:
            productName = 'Ffrees 7.5 - Free BACs';
            break;
        case 18:
        case 25:
            productName = 'Ffrees 10';
            break;
        default:
            productName = '';
    }

    return productName;
}

// Formates date into friendly format
Date.prototype.formatDate = function (returnedFormat) {

    var mon = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var regex;
    var format = returnedFormat;

    regex = new RegExp('yyyy', 'g');
    format = format.replace(regex, this.getFullYear());

    regex = new RegExp('MMM', 'g');
    format = format.replace(regex, mon[this.getMonth()]);

    regex = new RegExp('dd', 'g');
    format = format.replace(regex, this.getDate() + nth(this.getDate()));

    return format;

};

function nth(day) {
    if (day > 3 && day < 21) {

        return 'th';

    }

    switch (day % 10) {
        case 1: return 'st';
        case 2: return 'nd';
        case 3: return 'rd';
        default: return 'th';
    }
}

// Checks if a form input is supported on the load device
function supportsType(input) {
    var desiredType = input.attr('type');
    var supported = false;

    if (input[0].type === desiredType) {

        supported = true;

    }

    input.value = 'Hello world';

    var helloWorldAccepted = (input[0].value === 'Hello world');

    switch (desiredType) {
        case 'email':
        case 'url':

            if (!input.validationMessage) {

                supported = false;

            }

            break;

        case 'color':
        case 'date':
        case 'datetime':
        case 'month':
        case 'number':
        case 'time':
        case 'week':

            if (helloWorldAccepted) {

                supported = false;

            }

            break;
    }

    input.value = '';
    return supported;
}

//
// on scroll / resize stuff
//

$(window).scroll(function () {

    scrollPos = $(window).scrollTop();

    headerFooterOporations(scrollPos);

    stickyCheck();

}).scroll();

//
// on resize stuff
//

$(window).resize(function () {

    winWidth = $(window).innerWidth();
    windowHeight = $(window).height();
    bodyHeight = $('body').height();

    makeMq();
    headerFooterOporations();

    //sliderMove();

    findHeight($('.hiddenFields'));

    stickyCheck();

}).resize();

$(document).ready(function () {

    console.log('GRUNT Environment: DEV MODE - Please run "Grunt Prod" before committing to GIT');

    // ie hack to add ie 11 class

    if (navigator.msMaxTouchPoints !== void 0) {

        $('html').addClass('ie11');

    }

    // functions to run on page load

    makeMq();
    headerFooterOporations();
    loadCorrectTab();
    stickyCheck();

    //
    //  Main body of code
    //

    checkCookieAuth(); // cookie banner

    // accept cookie banner - set a cookie & close banner

    $('.acceptCookie').click(function () {

        document.cookie = cookieName + '=True; expires=Mon, 11 Nov 2777 11:11:00 GMT; path=/';
        $addPadding.css('margin-top', '0');
        $html.css('margin-top', '0');
        $cookieBanner.fadeOut();

    });

    if ($('#Glide').length) {

        var slider = $('#Glide').glide({

            autoplay: false,
            type: 'carousel',
            animationDuration: 500,

        });

    }

    //login button

    $(document).on('click', '.logInButton', function (e) {

        e.preventDefault();

        if (preventClick) {

            preventClick = false;

            $.ajax({ //make your ajax call to get content
                url: '/loginpart',
                data: { redirect: $(this).data('redirect') },
                success: function (data) {

                    makeModal({
                        title: 'Login',
                        body: data,
                        footerButtons: '',
                    });

                    preventClick = true;

                },
            });

        }

    });

    // run pin reveal depending upon browsers

    if ($('html').hasClass('ie10') || $('html').hasClass('ie11')) {

        $('.pinReveal').change(function (e) {

            pinReveal($(this));

        });

    } else {

        $('.pinReveal').on('input', function (e) {

            pinReveal($(this));

        });

    }

    // make links on trs

    $('tr').each(function () {

        var $this = $(this);

        if ($this.attr('data-link')) {

            var data = $this.attr('data-link');

            $this.css('cursor', 'pointer');

            $this.bind('click', function () {

                window.location.href = data;

            });
        }
    });

    // Horrible hack to fix canvas on android

    if (window.navigator && window.navigator.userAgent.indexOf('534.30') > 0) {

        var url = window.location.pathname;

        if (url != '/account') {

            $('canvas').parents('*').css('overflow', 'visible');

        }

    }

    // main line graph, have to change the color function later on

    $('.lineGraph').each(function (i) {

        var $this = $(this);

        var jamCurrent = parseInt($this.children('.lineGraphTotal').attr('data-value'), 10);

        var jamTotal = parseInt($this.children('.lineGraphTotal').attr('data-total'), 10);

        var jamWidth = jamCurrent / jamTotal * 100;

        if (jamWidth >= 50) {

            $this.parent().find('.jamJarBalance').addClass('green');

        } else {

            $this.parent().find('.jamJarBalance').addClass('orange');
            $this.children('.lineGraphTotal').addClass('orangeBg');

        }

        window.setTimeout(function () {

            $this.children('.lineGraphTotal').css('width', jamWidth + '%');

        }, 150 * i);

    });

    // dashboard balance flash

    $('.availableFlash').click(function () {

        var tableFlash = $('.myAccountTable:first-of-type').find('tr:nth-child(2) .flashBalance');

        tableFlash.addClass('flash');

        tableFlash.one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function () {

            $(this).removeClass('flash');

        });

    });

    $('.pendingFlash').click(function () {

        var tableFlash = $('.pendingFlashTable').find('tr.fade');

        tableFlash.addClass('backgroundFlash');

        tableFlash.one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function () {

            $(this).removeClass('backgroundFlash');

        });

    });

    //ToolTips

    $('.toolTipContainer').each(function () {

        var $this = $(this);
        var childSearch = $this.children('.toolContents');
        var arrowSearch = $this.find('.toolTipArrow');

        $this.hover(function () {

            var pos = $this.offset();
            var width =  childSearch.outerWidth();
            var widthAndPos = width + pos.left;
            var posHalf = pos.left - (width / 2);
            var marginTop = $('header').height();

            if (widthAndPos >= winWidth) {

                childSearch.css({ left: (winWidth - width - 20), top: ((pos.top + 45) - marginTop) });
                arrowSearch.css({ left: (pos.left - (winWidth - width - 30)) });

            } else if (posHalf <= 0) {

                childSearch.css({ left: 20, top: ((pos.top + 45) - marginTop) });
                arrowSearch.css({ left: (pos.left - 15) });

            } else {

                childSearch.css({ left: posHalf, top: (pos.top + 45 - marginTop) });
                arrowSearch.css({ left: 'calc(50% + 12.5px' });

            }

            childSearch.addClass('showTool');
            arrowSearch.addClass('showTool');

        }, function () {

            childSearch.removeClass('showTool');
            arrowSearch.removeClass('showTool');

        });

    });

    // modal for photo edit on my profile

    $('.photoEdit').click(function () {

        $this = $(this);

        makeModal({
            title: 'Select a photo location',
            body: '<p>Please select a location you would like to upload a photo from.<\/p><div class=\"photoAjax\"><a class=\"facebookLink\" href=\"\/dashboard\/profile\/facebookPhoto\"><span class=\"iconFacebook-squared\"><\/span><p>From Facebook<\/p><\/a><a href=\"\/dashboard\/profile\/uploadPhoto\"><span class=\"iconFolderOpen\"><\/span><p>From your computer<\/p><input class=\"fileUpload\" type=\"file\" accept=\"image\/*\"><\/a><\/div>',
        });

        facebookInit();

    });

    // open file upload dialogue

    $(document).on('change', '.fileUpload', function (e) {

        readURL(this, '/dashboard/profile/uploadPhoto');

    });

    // open facebook upload

    $(document).on('click', '.facebookLink', function (e) {

        e.preventDefault();

        facebookLogin();

    });

    // open cropper

    if ($('.img-container').length > 0) {

        var base64Value =  $.sessionStorage('image');

        $('.img-container > img').attr('src', base64Value);

        $('.img-container > img').cropper({

            aspectRatio: 1 / 1,
            autoCropArea: 0.75,
            minContainerWidth: 0,
            minContainerHeight: 0,
            background: false,
            guides: true,
            dragCrop: true,
            resizable: true,
            zoomable: false,
            crop: function (data) {

            },

        });

        $('.cropSubmit').click(function () {

            var image = $('.img-container > img').cropper('getDataURL', { width: 240, height: 240 });

            window.open(image, '_self');

        });

    }

    // prevent a user from pasting into a field
    $('.nopaste').on('paste', function (e) {
        e.preventDefault();
    });

    //
    // NEW JS
    //

    $(document).on('click', '.globalContainer.navOpen, header .iconMenu', function (e) {

        e.preventDefault();

        $('nav').toggleClass('navClosed');
        $('.dashboardNav').toggleClass('bottomNavClosed');
        $('.globalContainer, header, .floatingBar').toggleClass('navOpen');

    });

    // run tabs on page load and add listner

    window.addEventListener('hashchange', loadCorrectTab, false);

    //on click tab event to open correct tab + change hash - TABS

    $(document).on('click', '.tabButton', function (e) {

        var $this = $(this);

        var location = $this.attr('href');

        e.preventDefault();

        if ($this.hasClass('actualLink')) {

            window.location.replace(location);

        }

        if (!$this.hasClass('activeTabButton')) {

            if (history.pushState) {

                history.pushState(null, null, location);

            } else {

                window.location.hash = location;

            }

            $('.activeTabButton').removeClass('activeTabButton');

            $('a[href="' + location + '"]').addClass('activeTabButton');

            $('.tabCell').hide();

            $(location).show();

        }

    });

    // landing page tab open

    $('.landingPageOptions span').click(function () {

        var $this = $(this);
        var target = $this.attr('id');

        $('.landingPageOptions span').removeClass('orangeBg, whiteBg').addClass('whiteBg');
        $this.removeClass('whiteBg').addClass('orangeBg');

        $('.landingUnrivalled, .landingFiveReasons, .landingUnbanking').addClass('hidden');
        $('.' + target).removeClass('hidden');

    });

    $('.youTubeGo').click(function (e) {

        e.preventDefault();

        var id = $(this).attr('href');

        $('html, body').animate({ scrollTop: $(id).offset().top - $('header').height() }, 1000);

    });

    // add body class for animations to prevent jumping on page load

    window.setTimeout(function () {

        $('body').addClass('animation');

    }, 1000);

    // fees table below slider
    $('#feesTable').slideUp();

    // Fees Table slide up/down
    $('.showFees').click(function (e) {
        var $this = $(this);

        e.preventDefault();
        $('#feesTable').slideToggle();

        $this.text(function (i, text) {
            return text === 'Hide all Account Fees' ? 'Show all Account Fees' : 'Hide all Account Fees';
        });
    });

    // remove captcha fail

    $(document).on('click', '.visualCaptchaPossibilities, .visualCaptchaButtonGroup', function () {

        $('.visualCaptchaTarget .inputHint').remove();

    });

    // reveal security code

    $(document).on('change', '.revealPinToggle', function () {

        var $this = $(this);
        var $inputNodes = $this.closest('.sixDigitInput').find('input').not('.cmnToggle');

        if ($this.is(':checked')) {

            $inputNodes.attr('type', 'text');

        } else {

            $inputNodes.attr('type', 'password');

        }

    });

    // tab next on security code after inputting a digit

    $('.sixDigitInput input').not('.cmnToggle').on('keyup', function () {

        var $this = $(this);

        if ($this.val().length === 1) {

            $this.next('input').focus();

        }

    });

});
