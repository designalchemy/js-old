
function addCustomHint(target, message) {

    // adds a custom hint message

    var $errorHint = $('<div />', {
        class: 'inputHint showIt',
        text: message,
    });

    target.parents('label').removeClass('valid warning').addClass('invalid').append($errorHint);

}

function removeLabels(el, empty) {

    // remove labels + emptys if required

    var $this = el;

    var label = $this.closest('label');

    $this.each(function () {

        label.removeClass('invalid warning valid');

        if ($this.is('option') || $this.is('select')) {

            $this.closest('.selectInput').siblings('.requiredHint, .inputHint').remove();

        } else {

            $this.siblings('.requiredHint, .inputHint').remove();

        }

        if (empty) {

            // remove vals + strip small labels

            $this.val('');
            label.removeClass('smallLabel');

            if ($this.hasClass('datePicker')) {

                $this.siblings('.dpWrapper').find('.dpTag').remove();
            }

        }

    });

}

function setLabelsOnLoad() {

    //This part of code will detect autofill when the page is loading (username and password inputs for example)

    var loading = window.setInterval(function () {

        $('input').not('.dateFakeInput, .noValidate, [type="file"], #passwordInput, :disabled').each(function () {

            var $this = $(this);

            if ($this.val() != $this.attr('value') || !!$this.val()) {

                $this.trigger('change');

            }

            if ($this.attr('type') == 'password') {

                try {

                    if ($('#' + $this.attr('id') + ':-webkit-autofill').length === 1) {

                        $this.trigger('change');
                    }

                }

                catch (error) {

                    // handle fail

                }

            }

            formValidate($this);

        });

        $('select').not('.noValidate, #ttAddressSelection').each(function () {

            var $this = $(this);
            var $thisLabel = $this.parents('label');

            if ($this.val() !== null) {
                $thisLabel.addClass('valid');
                $this.trigger('change');
            }

            formValidate($this);

        });

    }, 100);

    window.setTimeout(function () {

        window.clearInterval(loading);

    }, 3500);

}

// on action puts the small labels on

function makeSmallLabel($element) {

    var $this = $element;
    var thisLabel = $this.parents('label');
    var labelText = thisLabel.find('span.label');

    if (labelText.hasClass('stay')) {
        // Use the class stay to keep the label where it is
        thisLabel.addClass('labelStay');

    } else if (labelText.hasClass('fadeOut')) {
        // class fadeOut Fades the label out
        thisLabel.addClass('fadeLabel');

    } else if (!thisLabel.hasClass('noLabel')) {
        // default is to shrink the label
        thisLabel.addClass('smallLabel');

    }

}

// on blur remove the small labels if empty

function removeSmallLabel($element) {

    var $this = $element;
    var thisLabel = $this.parents('label');
    var labelText = thisLabel.find('span.label');
    var datePickerInput = $this.hasClass('dateFakeInput');

    if (datePickerInput === false) {

        if ($('select')) {

            if (!$this.val()) {

                removeSmallLabelAction(labelText, thisLabel);

            }

        } else if (!$this.val() || $this.val() === 'null') {

            removeSmallLabelAction(labelText, thisLabel);

        }
    }
}

// the aciton for remove small labels from above

function removeSmallLabelAction(labelText, thisLabel) {

    if (labelText.hasClass('fadeOut')) {

        thisLabel.removeClass('fadeLabel');

    } else {

        thisLabel.removeClass('smallLabel');

    }
}

// Works out the position of a radio button and places the highlight behind it
function radioFunction(el) {

    var hightlightPos = el.parent('label').position().left;

    el.parents('div.radioGroup').find('div:last-of-type').css('left', hightlightPos);

}

$(window).resize(function () {
    // When the screen is resized the background will work out it new position
    $('.radioGroup :checked').each(function () {

        radioFunction($(this));

    });
});

$(document).ready(function () {

    // checks if there has been a value passed
    $('input').each(function () {

        var $this = $(this);

        //Check if form inputs are filled and show appropate label
        if ($this.val()) {

            makeSmallLabel($this);

        }

    });

    // sets labels on page load + auto fill

    if ($('input').length || $('select').length) {

        setLabelsOnLoad(); // sorts lables

    }

    // set small labels on change

    $(document).on('change', 'form :input', function () {
        makeSmallLabel($(this));
    });

    // Form Labels - script that controls the labels on focus

    $(document).on('focusin', 'form :input', function () {

        var $label = $(this).parents('label');

        makeSmallLabel($(this));

        if ($label.children().hasClass('inputHint')) {

            $label.children().removeClass('showIt');

        }

    });

    // So the input group will share left-border when focused

    $(document).on('focus', 'form :input', function () {

        $(this).parent('label').addClass('labelFocus');

    });

    // remove small labels on focus out

    $(document).on('blur', 'form :input', function () {

        $('label').removeClass('labelFocus');

    });

    $(document).on('focusout', 'form :input', function () {

        removeSmallLabel($(this));

    });

    // adds small label if selet has a value

    $('select').load(function () {

        var $this = $(this);

        if ($this.val()) {
            $this.parents('label').addClass('smallLabel');
        }

    });

    // Radio buttons sliding backgrounds

    $('.radioGroup :input').on('click', function () {

        radioFunction($(this));

    });

});
