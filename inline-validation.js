
var $form = $('form');

// Check if device has touch enabled
var msTouchEnabled = window.navigator.msMaxTouchPoints; // windows 10 and below
var generalTouchEnabled = 'ontouchstart' in document.createElement('div'); // All other modern browers

var i = 0;
var minPasswordLength = 8;
var maxPasswordLength = 20;
var minNumbers = 2;
var minCharacters = 1;
var inputLength = 4;

var _hasDigit = /\d/g;
var _hasDigitStr = /\d*/g;
var _hasLowerCase = /[a-z]/i;
var _hasUpperCase = /[A-Z]/;
var pounds = /^([1-9][0-9]{0,}|10000|([0-9]{1}\.([0-9][1-9]|[1-9][0-9])))(?:\.\d{2})?$/;
var _validEmail = /^\S+@\S+\.\S+$/;
var _validMobile = /^07[0-9]{9,10}$/;
var _validHousePhone = /^0[1-8][0-9]{8,9}$/;
var _validPostcode = /^((([A-PR-UWYZ])([0-9][0-9A-HJKS-UW]?))|(([A-PR-UWYZ][A-HK-Y])([0-9][0-9ABEHMNPRV-Y]?))\s{0,2}(([0-9])([ABD-HJLNP-UW-Z])([ABD-HJLNP-UW-Z])))|(((GI)(R))\s{0,2}((0)(A)(A)))$/i;

// match special characters except space
var _hasSpecial = /(_|[^\w\d ])/;
var input = [];
var number = [];
var timer;

var $hasSpecialListItem = $('#req-special');
var inputLabel;

var $hasLowerCaseListItem = $('#req-lower');
var $hasUpperCaseListItem = $('#req-upper');

// adds a required hint + label

function thisIsRequired($thisItem) {

    var requiredHint = '<div class="requiredHint">Please fill this in</div>';
    var $thisLabel = $thisItem.closest('label');

    $thisItem.find('.inputHint, .requiredHint').remove();

    $thisLabel.removeClass('invalid').addClass('warning');

    $(requiredHint).appendTo($thisLabel);

}

// Takes fullYearDate and makes it useful

function formatDate(date) {

    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yy = date.getFullYear();

    // formats day to a two digit number
    if (dd < 10) {
        dd = '0' + dd;
    }

    // formats month to two digit number
    if (mm < 10) {
        mm = '0' + mm;
    }

    return yy.toString() + mm.toString() + dd.toString(); // Matches date formate that the input[type="date"] will pass
}

/* check validation, adjust classes & show hint if required
 *      "has"         Can be true or false
 *      "$thisLabel"  Element valid/invalid class need to be added too (label)
 *      "$input"      needs to be the element that has data-hint included (input)
 */
function checkAndSwitchClasses(has, $thisLabel, $input) {

    var required;
    var thisHint;
    var thisId;
    var typeOfField;

    if ($input) { // Target does not have to be defined as not all checks require it

        thisHint = $input.data('input-hint'); // This finds the form elements hint if validation has not been forfilled

        thisId = $input.attr('id');
        typeOfField = $input.attr('type');

        if ($input.hasClass('dpTagBox')) {

            $input = $thisLabel.find('.datePicker');
            required = $input.prop('required');

        } else {

            required = $input.prop('required');

        }

    }

    // Used by date picker so hint isn't shown if empty
    if (has === 'nothing') {

        $thisLabel.removeClass('invalid valid').children('.inputHint').detach();

        if (required) {

            thisIsRequired($input);

        }

        return false;

    } else if (has) { // If validation has passed

        $thisLabel.removeClass('invalid warning').addClass('valid');
        $('.requiredHint, .inputHint', $thisLabel).remove(); // hides warning hint

        return true;

    } else if (typeOfField == 'password') { // If password auto completed

        try {

            if ($input.filter(':-webkit-autofill').length === 1) {

                $thisLabel.removeClass('invalid warning').addClass('valid');
                $('.requiredHint, .inputHint', $thisLabel).remove(); // hides warning hint

                return true;

            }

        }

        catch (error) {

            // handle fail

        }

    }

    if ($input === undefined || $input.val() !== '') {
        $thisLabel.removeClass('valid warning').addClass('invalid'); // If validation is failed
    }

    $('.requiredHint', $thisLabel).remove();

    if ($input) {

        if (!$input.val() && required) {

            thisIsRequired($input);

        } else if (!$thisLabel.find('.inputHint').length && $input.val() !== '') {

            $thisLabel.removeClass('warning').addClass('invalid').append('<div class="inputHint showIt">' + thisHint + '</div>');
            $('.requiredHint', $thisLabel).remove();

        }
    }

    return false;
}

// Enforces server side password rules on the client for convenience

function enforcePassword(el) {

    var $thisPw = el;
    var pw = $thisPw.val().toLowerCase();
    var passwordCorrect = $thisPw.parents('label');
    var pwRightLength = passwordCorrect.find('.reqLength');
    var pwRightDigits = passwordCorrect.find('.reqDigit');
    var pwRightMatches = passwordCorrect.find('.reqMatches');
    var pwRightCharacters = passwordCorrect.find('.reqCharacters');
    var digitNum = pw.match(_hasDigit); // Finds just the numbers in the password field
    var characters = pw.match(_hasLowerCase);
    var hasDigits;
    var pwMatches = true;
    var hasRight = pw.length >= minPasswordLength && pw.length <= maxPasswordLength;
    var hasCharacters;

    if (characters) {
        hasCharacters = characters.length >= minCharacters;
    }

    if (digitNum) { // Errors are caused if no numbers are present

        hasDigits = digitNum.length >= minNumbers; // Checks the right number of digits are present

    }

    if (pwRightMatches.length) {

        pwMatches = $('#password').val() === $thisPw.val();

    }

    var passChecked = (hasRight && hasDigits && pwMatches && hasCharacters); // Checking if both requirements have been fulfilled

    /* These are not used at the moment but maybe useful in the future
     * var hasLower = _hasLowerCase.test(password.val());
     * var hasUpper = _hasUpperCase.test(password.val());
     * var hasSpecial = _hasSpecial.test(password.val());
     */

    $('#password.invalid').removeClass('invalid warning');

    checkAndSwitchClasses(hasCharacters, pwRightCharacters);
    checkAndSwitchClasses(hasRight, pwRightLength);
    checkAndSwitchClasses(hasDigits, pwRightDigits);
    checkAndSwitchClasses(pwMatches, pwRightMatches);
    checkAndSwitchClasses(passChecked, passwordCorrect);

    /* These are not used at the moment but maybe useful in the future
     * checkAndSwitchClasses(hasLower, $hasLowerCaseListItem);
     * checkAndSwitchClasses(hasUpper, $hasUpperCaseListItem);
     * checkAndSwitchClasses(hasSpecial, $hasSpecialListItem);
     */

    // if ( pw.length === 0 && !password.prop('required') ) $('.invalid').removeClass('invalid');
    // Format of multiple requirements
    //  if (!(hasEight && hasLower && hasUpper && hasDigit && hasSpecial)) {
    //  return false;
    //}
    // Use if two inputs must match
    // if (mustMatch() && (hasEight && hasLower && hasUpper && hasDigit && hasSpecial))
}

// checks length of input field and prevents if false

function checkLength($input) {

    var valueLength = $input.val().length;
    var maxInputLength = $input.data('input-max');
    var minInputLength = $input.data('input-min');
    var exactInputLength = $input.data('input-exact');
    var maxLength;
    var minLength;
    var exactLength;
    var hasInput;

    if (maxInputLength > 0) {
        maxLength = maxInputLength;
    }

    if (minInputLength > 0) {
        minLength = minInputLength;
    }

    if (exactInputLength > 0) {
        exactLength = exactInputLength;
    }

    if (maxLength && minLength) {
        hasInput = valueLength <= maxLength && valueLength >= minLength;
    } else if (maxLength) {
        hasInput = valueLength <= maxLength && valueLength > 0;
    } else if (minLength) {
        hasInput = valueLength >= minLength;
    } else if (exactLength) {
        hasInput = valueLength === exactLength;
    } else {
        hasInput = valueLength;
    }

    return hasInput;
}

// checks if the fields are valid

function enforceInput($this) {

    var inputLabel = $this.parents('label');
    var hasInput = checkLength($this);

    inputLabel.removeClass('warning invalid valid');

    checkAndSwitchClasses(hasInput, inputLabel, $this);

}

// checks for the min and max value

function enforceMinMax(el) {

    var $this = el;
    var thisValue = $this.val();
    var minValue = $this.data('min-value');
    var maxValue = $this.data('max-value');
    var valid;

    if (minValue && (thisValue < minValue)) {

        valid = false;

    } else if (maxValue && (thisValue > maxValue)) {

        valid = false;

    } else {

        valid = true;

    }

    return valid;

}

function enforceBirthDate(el) {

    var newDate = new Date();
    var validBirthDate = newDate.setFullYear(newDate.getFullYear() - 18); // takes current date and minus 18 years
    var birthDate = el.val().replace(/-/g, ''); // Takes out dashes pass by input[type="date"]

    var dateValid;

    if (birthDate === '' || birthDate === 'undefinedundefinedundefined') {

        dateValid = 'nothing';

    } else {

        dateValid = Boolean(birthDate <= formatDate(newDate));

    }

    checkAndSwitchClasses(dateValid, el.parent('label'), el);

}

function enforceDate($this) {

    if (($this.hasClass('firstPaymentDate') && $('.secondPaymentDate').val().length > 0) || ($this.hasClass('secondPaymentDate') && $('.firstPaymentDate').val().length > 0)) {

        enforceDateRange($this);

    } else {

        enforceFutureAndPastDates($this);

    }

    formValidate($this);

}

// stops end dates being before start dates

function enforceFutureAndPastDates($this) {

    var todaysDate = new Date();
    var dateValid;
    var thisDateValue = $this.val().replace(/-/g, '');
    var historyData = $this.data('history');

    if (thisDateValue === '' || thisDateValue === 'undefinedundefinedundefined') {

        dateValid = 'nothing';

    } else {

        if (historyData === ('dayInFuture')) {

            todaysDate.setDate(todaysDate.getDate() + 1);

        } else if (historyData === ('dayInPast')) {

            todaysDate.setDate(todaysDate.getDate() - 1);

        }

        var date = formatDate(todaysDate);

        if (historyData === 'future' || historyData === 'dayInFuture') {

            // Future Date

            dateValid = thisDateValue >= date;

        } else if (historyData === 'past' || historyData === 'dayInPast') {

            //Past Date

            dateValid = thisDateValue <= date;

        }

    }

    checkAndSwitchClasses(dateValid, $this.parents('label'), $this);
}

function enforceDateRange($this) {

    var $firstPaymentDate = $('.firstPaymentDate');
    var $secondPaymentDate = $('.secondPaymentDate');

    var firstDateValue = $firstPaymentDate.val().replace(/-/g, '');
    var secondDateValue = $secondPaymentDate.val().replace(/-/g, '');

    removeLabels($firstPaymentDate);
    removeLabels($secondPaymentDate);

    if (firstDateValue >= secondDateValue) {

        addCustomHint($firstPaymentDate, 'Start date must be before end date.');
        addCustomHint($secondPaymentDate, 'Start date must be before end date.');

    } else {

        enforceFutureAndPastDates($firstPaymentDate);
        enforceFutureAndPastDates($secondPaymentDate);
        submitRange($this);

    }

}

// Function that validates using regEx defined at the top of this doc
// Every new type will need adding

function enforceRegEx(el) {

    var input = el;
    var inputLabel = input.parents('label');
    var value = input.val();
    var regExValidate = input.data('reg-match');
    var poundsCheck = value.match(pounds);
    var mobNumber = value.match(_validMobile);
    var emailAddress = value.match(_validEmail);
    var phoneNumber = value.match(_validHousePhone);
    var postcode = value.match(_validPostcode);
    var phoneOrMob;
    var checkValLength = checkLength(input);
    var minMax = enforceMinMax(input);
    var check = false;

    inputLabel.removeClass('warning invalid valid');

    if (regExValidate === 'pounds') {

        if (minMax && poundsCheck) {

            check = true;

        }

        checkAndSwitchClasses(check, inputLabel, input);

    }

    if (regExValidate === 'mobile') {

        checkAndSwitchClasses(mobNumber, inputLabel, input);

    }

    if (regExValidate === 'phoneOrMob') {

        if (mobNumber || phoneNumber) {
            phoneOrMob = true;
        }

        checkAndSwitchClasses(phoneOrMob, inputLabel, input);

    }

    if (regExValidate === 'phone') {

        checkAndSwitchClasses(phoneNumber, inputLabel, input);

    }

    if (regExValidate === 'email') {

        checkAndSwitchClasses(emailAddress, inputLabel, input);

    }

    if (regExValidate === 'postcode') {

        if (checkValLength && postcode) {

            check = true;

        } else {

            check = false;
        }

        checkAndSwitchClasses(check, inputLabel, input);

    }
}

function onTypingInput($this) {

    var typingTimer;
    var doneTypingInterval = 50;
    var finaldoneTypingInterval = 400;
    var mobile = $this.hasClass('mobile');
    var dataType = $this.data('type');
    var type;

    if (dataType) {
        type = dataType;
    } else {
        type = 'input';
    }

    $this.on('keydown', function (e) {
        var charCode = e.charCode || e.keyCode;

        if ($this.data('limit-input')) {
            isCharKey(e);
        }

        if ($this.data('input-max') || $this.data('input-exact')) {
            restrictInputValue($this, e);
        }

        if (charCode !== 9) {

            clearTimeout(typingTimer);

            if ($this.val) {
                typingTimer = setTimeout(doneTypingInterval);
            }
        }

    });

    $this.on('keypress', function (e) {

        if ($this.data('limit-input')) {
            isCharKey(e);
        }

    });

    $this.on('keyup', function (e) {

        var charCode = e.charCode || e.keyCode;

        if (charCode !== 9) {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {

                switchCorrectType($this, type);

            }, finaldoneTypingInterval);
        }

    });

    $this.on('change', function () {

        switchCorrectType($this, type);

    });

    $this.on('blur', function () {

        switchCorrectType($this, type);

    });

}

function switchCorrectType($this, type) {

    switch (type) {

        case 'input':
            enforceInput($this);
            break;

        case 'regMatch':
            enforceRegEx($this);
            break;

        case 'password':
            enforcePassword($this);
            break;

        default:
            break;

    }

    formValidate($this);

}

// adds or removes the disabled view from the submit button

function formValidate($valObj) {

    var thisHasFailed = false;
    var thisForm = $valObj.parents('form');
    var buttonTarget = thisForm.find('button').not('#ttPostcodeSearch');

    thisForm.find('input, select, textarea').not('.cmnToggle, #redirect').each(function () {

        var $this = $(this);
        var required = $this.prop('required');
        var $thisLabel = $this.parents('label');

        if (required) {

            if ($this.val() === null || $this.val().length === 0 || $thisLabel.hasClass('invalid')) {

                thisHasFailed = true;

                if ($this.attr('type') === 'password') {

                    try {

                        if ($this.filter(':-webkit-autofill').length === 1) {

                            thisHasFailed = false;

                        }

                    }

                    catch (error) {

                        // handle fail

                    }
                }
            }
        }

    });

    if (thisForm.find('.visualCaptcha').length && thisForm.find('.visualCaptchaSelected').length === 0) {

        thisHasFailed = true;

    }

    if (thisHasFailed) {

        buttonTarget.addClass('submitDisabled');

    } else {

        buttonTarget.removeClass('submitDisabled');

    }

}

// Function that allows the number of characters to be limited
function restrictInputValue(el, event) {

    var maxInputLength = el.data('input-max');
    var expectedLength = maxInputLength ? maxInputLength : el.data('input-exact');
    var charCode = event.charCode || event.keyCode;

    var allowedKeys = [
        8, 9, 13, 37, 39, 46,
    ];

    /*
        backspace - 8
        tab - 9
        enter - 13
        left - 37
        right - 39
        delete - 46

    */

    if (expectedLength && el.val().length >= expectedLength && allowedKeys.indexOf(charCode) < 0) {

        event.preventDefault();

    }

}

// Allows only numbers to be used in a input box
function isNumberKey(evt, input) {

    var contis = $('body').hasClass('contisPages');
    var ctrl = evt.ctrlKey || evt.metaKey;
    var charCode = evt.charCode || evt.keyCode;
    var inputType = $(this).data('type');

    /*
        char - charCode
        . - 46
        0 - 48
        1 - 49
        2 - 50
        3 - 51
        4 - 52
        5 - 53
        6 - 54
        7 - 55
        8 - 56
        9 - 57
        backspace - 8
        left - 37
        right - 39
        delete - 46
        enter - 13
        tab - 9
    */

    var allowedKeys = [
        8, 9, 13, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
    ];

    if (!contis && inputType == 'number') {

        allowedKeys.push(46);

    }

    if (!ctrl) {

        if (allowedKeys.indexOf(charCode) < 0) {

            return false;
        }

    }

    return true;

}

function isCharKey(e) {

    var char;
    var evt = (e) ? e : window.event; //IE reports window.event not arg

    if (evt.type == 'keydown') {

        char = evt.keyCode;

        if (char < 16 || (char > 16 && char < 32) || (char > 32 && char < 41) || char == 46) {

            // non printables
            // avoid shift
            // navigation keys
            // Delete Key (Add to these if you need)

            nonChar = true;

        } else {

            nonChar = false;

        }

    } else {

        // This is keypress
        if (nonChar) {
            return;
        }

        // Already Handled on keydown
        char = (evt.charCode) ? evt.charCode : evt.keyCode;

        var allowedLetters = [
            0, 8,
            39, 45, 32,
            65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
            97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122,
        ];

        if (allowedLetters.indexOf(char) < 0) {

            evt.preventDefault();

        }

    }

}

$(document).ready(function () {

    // Checks toggle switches
    $('.cmnToggle').each(function () {

        var $this = $(this);
        $this.change(formValidate($this));

    });

    $('select').not('.noValidate').change(function () {

        var $this = $(this);
        var $thisLabel = $this.parents('label');

        if ($this.prop('required')) {

            if ($thisLabel.hasClass('invalid') || $thisLabel.hasClass('warning')) {
                $thisLabel.removeClass('invalid, warning');
                $thisLabel.find('.requiredHint').remove();
            }

            if ($this.val()) {
                $thisLabel.addClass('valid');
            }

            formValidate($this);

        }

    });

    // All over inputs that need to be validated
    $('.validateInput').each(function () {

        onTypingInput($(this));

    });

    $('.visualCaptcha').click(function () {

        formValidate($(this));

    });

    // This makes sure that only numbers can be put in to a number field

    $('.numberOnly, input[type="number"], input[type="tel"]').keypress(isNumberKey);

});
