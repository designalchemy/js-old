var firstRun = false;

// Any element with this class will be hidden and it's height put in a data element
var $slider = $('.hiddenFields');

//==========================================================
// FORMS
//==========================================================

// checks if required's are filled in + they have no warnings

function checkCanAjax($thisForm) {

    var requiredItems = $thisForm.find('input, textarea, select');

    var errorsTrue = false;

    requiredItems.each(function () {

        var $this = $(this);

        var $thisLabel = $this.closest('label');

        if ($this.prop('required') && (!$this.val() || $this.val() === null)) {

            thisIsRequired($this);

            errorsTrue = true;

        }

        if ($thisLabel.hasClass('warning') || $thisLabel.hasClass('invalid')) {

            errorsTrue = true;

        }

    });

    if (errorsTrue) {

        $thisForm.find('.genericAjax, .errorCheckDefaultAction').addClass('submitDisabled').attr('disabled', false); // prevents double click and visually disabled button

        goToFirstError();

        return false;

    } else {

        $thisForm.find('.genericAjax, .errorCheckDefaultAction').removeClass('submitDisabled').attr('disabled', false); // prevents double click and visually disabled button

        return true;

    }

}

// scrolls to first error on page

function goToFirstError() {

    $('body, html').animate({ scrollTop: $('.inputHint, .requiredHint').eq(0).offset().top - $('header').height() - 70 }, 'fast', 'linear');

}

// generic ajax action

function genericAjax($thisForm) {

    var sumbitButton = $thisForm.find('.genericAjax');

    sumbitButton.addClass('iconSpinSix spinIcon');

    $.ajax({
        url: $thisForm.attr('action'),
        type: 'POST',
        data: $thisForm.serialize(),

        success: function (response) {

            if (response.success) {

                // return turn from back end

                if (response.redirect) {

                    // if back end has a redirect json response

                    window.location.replace(response.redirect);

                } else {

                    flashMessage('success', response.message);

                    $thisForm.find('.genericAjax, .errorCheckDefaultAction').removeClass('submitDisabled').attr('disabled', false);

                }

            } else {

                // return error from back end

                handleError(response);

            }
        },

        error: function () {

            flashMessage('error', 'An unknown error occurred');

        },

        complete: function () {

            sumbitButton.removeClass('iconSpinSix spinIcon');

        },

    });

}

// deals with errors, throws custom back end errors to the correct input field

function handleError(data) {

    var error;
    var errorObject;

    $('.errorMessage').remove();

    if (data.redirect) {

        // if back end has a redirect json response

        window.location.replace(data.redirect);

        return false;

    }

    // loop thught error messages and appy where needed

    if (data.hasOwnProperty('fieldErrors')) {

        errorObject = data.fieldErrors;

        Object.keys(errorObject).forEach(function (key) {

            appendFieldErrors(key, errorObject[key]);

        });

    } else if (data.hasOwnProperty('errorFields')) {

        errorObject = data.errorFields;

        error = 'This entry is invalid. Please check it and try again.';

        Object.keys(errorObject).forEach(function (key) {

            appendFieldErrors(errorObject[key], error);

        });

    } else {

        if (data === 'fail') {

            error = 'An unknown error occurred';

        } else {

            error = data.error;

        }

        flashMessage('error', error);

    }

}

// appends custom back end errors messages

function appendFieldErrors(targetName, error) {

    if (targetName === 'captcha') {

        var $catchaError = $('<div />', {
            class: 'inputHint showIt',
            text: 'The captcha you\'ve entered is incorrect. Please try again.',
        });

        $('.visualCaptcha-refresh-button').click();

        $('.visualCaptchaTarget').append($catchaError);

    } else {

        var $errorHint = $('<div />', {
            class: 'inputHint showIt',
            text: error,
        });

        var $target = $('[name="' + targetName + '"]');

        if ($target.attr('data-submit-error') !== undefined) {

            $errorHint = $('<div />', {
                class: 'inputHint showIt',
                text: $target.data('submit-error'),
            });

        }

        $target.parents('label').removeClass('valid').addClass('invalid').append($errorHint);

    }

    goToFirstError();

}

// flash message function type = warning, error or success

function flashMessage(type, text) {

    var $target = $('.errorContainer');
    var $errorDiv = $('<div class="errorMessage" />');
    var $warningDiv = $('<div class="warningMessage" />');
    var $successDiv = $('<div class="successMessage" />');
    var $infoDiv = $('<div class="infoMessage" />');
    var send;

    $target.empty();

    if (type == 'error') {

        $errorDiv.html(text);
        send = $errorDiv;

    } else if (type == 'warning') {

        $warningDiv.html(text);
        send = $warningDiv;

    } else if (type == 'success') {

        $successDiv.html(text);
        send = $successDiv;

    } else if (type == 'info') {

        $infoDiv.html(text);
        send = $infoDiv;

    }

    $target.append(send);

}

// Make a nicer array than just serializing a form

function serializeObject(data) {

    var object = {};
    var serializeIt = data.serializeArray();

    $.each(serializeIt, function () {

        if (object[this.name] !== undefined) {

            if (!object[this.name].push) {

                object[this.name] = [object[this.name]];

            }

            object[this.name].push(this.value || '');

        } else {

            object[this.name] = this.value || '';

        }

    });

    return object;
}

// checks that the value in both the selects is valid before showing the hidden content

function showForm() {

    var fromAccountValue = $('.sendPaymentFrom').val();
    var toAccountValue = $('.sendPaymentTo').val();

    var $slider = $('.hiddenFields');
    if (fromAccountValue && toAccountValue && firstRun === false) {
        transitionHeight($slider);
        firstRun = true;
    }

}

// creates a new input field for address

function newInput(number, addressType, addressName) {
    var thisNewInput = [];

    thisNewInput.push('<label class="optAddress"><span class="label">Address Line ' + number + '<sup>optional</sup></span><input name="' + addressName + '" type="text" value="" data-address="' + addressType + '" /></label>');

    return thisNewInput.join('');
}

// Postcode Ajax call

function postcodeLookUp($this) {
    var $input = $this.contents().find('input');
    var $select = $this.next('div').contents().find('select');
    var $btn = $this.contents().find('button');
    var postcode = $input.val();

    $.ajax({
        url: '/ajax/address/postcode-lookup',
        data: { postcode: postcode },

        beforeSend: function () {
            $btn.addClass('loading');
        },

        complete: function () {
            $btn.removeClass('loading');
        },

        success: function (data) {
            if (data.success) {
                var options = data.options;
                var selectContainer = $select.parents('.chooseAddress');

                $select.empty();
                $select.append($('<option selected disabled></option><option class="view" disabled>Please pick an address</option>'));

                for (var id in options) {
                    var text = options[id];
                    var $option = $('<option>');
                    $option.text(text);
                    $option.val(id);
                    $select.append($option);
                }

                selectContainer.height(0).removeClass('hidden');

                setTimeout(function () {
                    selectContainer.height(selectContainer.data('height'));
                }, 1);

                if ($('body').hasClass('join')) {

                    $flex.height('auto');

                }

            } else {

                $input.parent('label').addClass('warning');

            }
        },
    });
}

// function to fire after valid date range

function submitRange($this) {

    if ($this.attr('id').substr(0, 21) === 'statementDateSelector') {

        statementSubmit($this);

    }

}

// Toggle Checkbox function
// $toggle -> the checkbox input
// $element -> the element to affect
function switchToggle($toggle, $element) {

    var requiredData = $toggle.data('required');
    var findInputs = $element.find(':input');

    if (requiredData === 'yes') {

        required = true;

    }

    if ($toggle.is(':checked')) {

        $element.removeClass('visHidden').addClass('visShown');
        transEnd($element, 1);

        if (required === true) {

            findInputs.each(function () {

                if (!$(this).data('not-required')) {
                    $(this).not('.dateFakeInput').prop('required', true);
                }
            });

            $toggle.trigger('change');

        }

    } else {

        $element.removeClass('visShown').addClass('visHidden');
        $element.find('label').removeClass('valid invalid warning smallLabel');

        if (required === true) {

            $element.find(':input').removeAttr('required');
            $element.find(':input').val('');
            $element.find('.dpTagBox span').remove();

        }

        $toggle.trigger('change');

    }

}

// To slide an element down we need to find the height and then hide it

function findHeight(el) {

    var $slideEl = el;

    $slideEl.addClass('hideHeight');
    $slideEl.height('auto').each(function () {

        $slideEl.attr('data-height', $slideEl.height());

    }).height('0');

}

// used on show form

function transitionHeight(el, trigger) {
    var $slideEl = el;
    var fired;

    if (!$slideEl.hasClass('hideHeight')) {

        fired = 0;

        $slideEl.addClass('hideHeight').height('0').css('overflow', 'hidden');
        transEnd($slideEl, fired);

    } else {

        fired = 1;

        if ($slideEl.hasClass('hiddenFields')) {
            transEnd($slideEl, fired);
        }

        $slideEl.removeClass('hideHeight').height($slideEl.attr('data-height'));

    }

    if (trigger) {

        // This hasn't been defined, needs fixing $this = trigger;?
        trigger.toggleClass('close');

    }

}

function removeDefaultFormValidation() {

    // prevent lots of binds if this is called multiple times

    document.querySelector('form').removeEventListener('invalid', function (e) {
        e.preventDefault();
    }, true);

    document.querySelector('form').addEventListener('invalid', function (e) {
        e.preventDefault();
    }, true);

}

$(document).ajaxStart(function () {

    $('.ajaxLoadingDiv').show();
    $('html, body').css('cursor', 'progress');

}).ajaxStop(function () {

    $('.ajaxLoadingDiv').hide();
    $('html, body').css('cursor', 'auto');

});

$(document).ready(function () {

    // global generic ajax call, add this class to button within form

    $('.genericAjax').click(function (e) {

        e.preventDefault();

        var $this = $(this);

        var $thisForm = $this.closest('form');

        $this.attr('disabled', true); // disable button on send to prevent double click

        if (checkCanAjax($thisForm)) {

            genericAjax($thisForm);

        }

    });

    // action call that checks the form for errors but uses default action

    $(document).on('click', '.errorCheckDefaultAction', function (e) {

        var $this = $(this);

        var $thisForm = $this.closest('form');

        $('.errorContainer').empty(); // will clear the error container

        $this.attr('disabled', true); // disable button on send to prevent double click

        if (!checkCanAjax($thisForm)) {

            e.preventDefault();

        }

    });

    // Show password text toggle button

    $('.protectedText').each(function (index, input) {
        var $input = $(input);
        var showBtnId = $(this).data('show');

        $('#' + showBtnId).click(function () {
            var change;

            if ($(this).is(':checked')) {

                change = 'text';

            } else {

                change = 'password';

            }

            var rep = $('<input type="' + change + '" />')
                .attr({
                    id: $input.attr('id'),
                    name: $input.attr('name'),
                    'data-show': $input.attr('data-show'),
                    class: $input.attr('class'),
                    autocomplete: $input.attr('autocomplete'),
                    required: true,
                    autocapitalize: 'none',
                    'data-input-max': $input.data('input-max'),
                    'data-type': $input.data('type'),
                })
                .val($input.val())
                .insertBefore($input);

            $input.remove();
            $input = rep;
            onTypingInput($input);

        });
    });

    //Stops browsers doing their own validation. // MAKE THIS INFO A FUNCTION FOR AJAX STUFF

    if ($('.globalContainer form').length) {

        removeDefaultFormValidation();

    }

    //
    // Address JS
    //
    // Post-code anywhere

    $('[data-address-lookup]').each(function () {
        var $this = $(this);
        var btn = $this.contents().find('button');
        var select = $this.next('div').contents().find('select');
        var address = $this.data('address-lookup');

        btn.click(function (e) {
            e.preventDefault();

            if (!btn.hasClass('disabled')) {
                postcodeLookUp($this);
            }
        });

        select.change(function () {
            var addressId = select.val();

            if (addressId !== '') {

                $.ajax({
                    url: '/ajax/address/address-lookup',
                    data: { addressId: addressId },
                    success: function (data) {
                        var $manualAddress;

                        for (var key in data.address) {

                            var value = data.address[key];
                            var $field = $('[data-address=' + address + '][data-address-field=' + key + ']');
                            var maxlength = $field.attr('maxlength');

                            $manualAddress = $('.showManualAddress[data-address=' + address + ']');

                            if (maxlength !== undefined) {
                                value = value.substr(0, maxlength);
                            }

                            $field.val(value);
                            $field.trigger('change');

                        }

                        if ($manualAddress) {

                            if (!$manualAddress.prop('checked')) {

                                $manualAddress.click();

                            }

                        }
                    },
                });
            }
        });

    });

    // Finds the height of the manual address holder and puts it in a data element and then sets it to zero
    $('.manualAddress').attr({ 'data-height': $('.manualAddress').height() }).height(0);

    // opens the manual add address sections

    $('.showManualAddress').each(function () {

        var $this = $(this);
        var $manualAddressHolder = $this.parents('.toShowAddress');
        var $manualAddress = $manualAddressHolder.children('.manualAddress');
        var addressType = $this.attr('data-address');
        var $addressSelect = $('#' + addressType + 'AddressSelect select');
        var $addressSelectParent = $addressSelect.parents('.chooseAddress');

        $this.on('click', function () {

            if ($this.is(':checked')) {

                // When manual adress is selected PostcodeAnywhere not required
                $addressSelect.prop('required', false);

                $addressSelectParent.height(0).on('webkitTransitionEnd mozTransitionEnd transitionend', function () {

                    $addressSelectParent.addClass('hidden');

                });

                $addressSelectParent.off('webkitTransitionEnd mozTransitionEnd transitionend');

                $manualAddress.removeClass('hidden').height($manualAddress.data('height'));

                $(':input', $manualAddress).each(function () {

                    if (addressType === 'primary') {

                        $(this).prop('required', true);
                        $('#' + addressType + 'County').prop('required', false);

                    }

                    if ($('body').hasClass('profile')) {

                        $(this).trigger('change');

                    }

                });

                $manualAddress.off('webkitTransitionEnd mozTransitionEnd transitionend');

            } else {

                $manualAddress.height(0);

                $addressSelect.prop('required', true);

                // fires once transition has finished, resetting things
                $manualAddress.on('transitionend webkitTransitionEnd mozTransitionEnd', function (e) {
                    $manualAddress.addClass('hidden');
                    $('.optAddress', $manualAddress).remove();
                    $('.addAddressLine', $manualAddress).removeClass('disabled');
                });

                $(':input', $manualAddress).each(function () {
                    var $this = $(this);
                    var $thisLabel = $this.parents('label');

                    $this.prop('required', false);
                    $thisLabel.removeClass('warning valid invalid');
                    $('.inputHint, .requiredHint', $thisLabel).remove();

                });

                // Check to see if Postcode Anywhere has been selected
                if ($('option', $addressSelect).length) {
                    $addressSelectParent.removeClass('hidden').height($addressSelectParent.attr('data-height'));
                }
            }
        });

    });

    $('.checkBalance').on('keyup change', function (e) {

        // used on payments and jamjars to check balance of accounts against payment ammount

        var $this = $(this);

        var $selectTarget;
        var selectOptionTarget;
        var inputTarget;

        var payments = $('.sendPayment').length === 1;

        if ($this.is('select')) {

            // if this is a select set the targers

            selectOptionTarget = $this.find(':selected');
            inputTarget = $this.closest('form').find('[name="' + $this.data('balance-target') + '"]');

            if ($this.val()) {
                removeLabels(selectOptionTarget);
                $this.closest('label').addClass('valid');
            }

        } else {

            // if this is a input field set tagergs

            $selectTarget = $('[data-balance-target="' + $this.attr('name') + '"]');

            selectOptionTarget = $selectTarget.find(':selected');

            inputTarget = $this;

            if (!payments) {

                // if this is jamjars (works differently)

                if (!inputTarget.val()) {

                    // if no value in input, make the field none required and set labels

                    $selectTarget.attr('required', false);
                    $('.blankSelect').prop('selected', true);
                    $selectTarget.prev('.label').find('sup').fadeIn();
                    removeLabels($selectTarget, true);

                } else {

                    // if value in input, make both required

                    $selectTarget.attr('required', true);
                    $selectTarget.prev('.label').find('sup').fadeOut();

                }

            } else {

                // if this is payments, make the from account valid

                selectOptionTarget.closest('label').removeClass('invalid').addClass('valid').find('.inputHint').remove();

            }
        }

        var accountBalance = parseFloat(selectOptionTarget.data('account-balance'));
        var userAmount = parseFloat(inputTarget.val());

        if (isNaN(accountBalance) || isNaN(userAmount)) {

            // if one field isnt filled in

            formValidate($this);
            return false;

        } else if (accountBalance < userAmount) {

            // if the amount is less than the balance
            removeLabels(selectOptionTarget);
            addCustomHint(selectOptionTarget, 'Balance on this source is not enough.');

        } else {

            // if we are good

            removeLabels(selectOptionTarget);
            selectOptionTarget.parents('label').addClass('valid');

        }

        formValidate($this);

    });

    $('#regularFundsToggle').click(function () {

        var $this = $(this);
        var $element = $('#regularFunds');
        var required = $this.data('required');

        switchToggle($this, $element, required);
        formValidate($this);

    });

});
