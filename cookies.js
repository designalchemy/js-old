var $cookieBanner = $('.cookieBanner');
var $addPadding = $('header');
var cookieName = 'cookieBannerAccepted';
var fundingCookieName = 'fundingBannerDismissed';
var $html = $('html');
var productCookieName = 'sliderSelectedProductId';
var cookieValue;

// check if there is a cookied, if not show the banner & set paddding

function checkCookieAuth() {

    if (document.cookie.indexOf(cookieName) == -1) {

        addPaddingToBody();
        $cookieBanner.show();

    }

    if (document.cookie.indexOf(fundingCookieName) == -1) {

        $('.fundingContainer').show();

    }

}

// add padding to sections to move the page down

function addPaddingToBody() {

    $addPadding.css('margin-top', $cookieBanner.outerHeight() + 'px');

    // check to see if service status banner is on + set body padding

    if ($('.serviceStatusBanner')) {
        $html.css('margin-top', $cookieBanner.outerHeight() + 'px');
    }

}

// check product cookie

function checkProductCookie() {

    if ($('div#productId').length) {

        var dataValue = $('div#productId').data('product-id');

        cookieValue = dataValue;
        makeLongCookie(productCookieName, dataValue);
        return true;

    } else if (document.cookie.indexOf(productCookieName) > 0) {

        cookieValue = getCookie(productCookieName);
        return true;

    } else {

        return false;

    }

}

function getCookie(cname) {
    var name = cname + '=';
    var cookieSplit = document.cookie.split(';');

    for (var i = 0; i < cookieSplit.length; i++) {
        var cookieIndex = cookieSplit[i];

        while (cookieIndex.charAt(0) == ' ') {

            cookieIndex = cookieIndex.substring(1);

        }

        if (cookieIndex.indexOf(name) === 0) {

            return cookieIndex.substring(name.length, cookieIndex.length);

        }
    }

    return '';
}

// on reisze set the padding correctly

$(window).resize(function () {
    checkCookieAuth();
});
