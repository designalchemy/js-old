// Modal plug-in
//
//  Options:
//      title: specify a title, it will appear in a h3
//      body: specify body content, wont appear in tags by default
//      footerButtons: specify footer buttons, will appear in buttonContainer but no markup by default
//      refresh: this adds a refresh function to the close button
//

// start making the modal

function makeModal($options) {

    var modalLocation = '/modals/modal';

    getModal(modalLocation, $options);

}

// grab the partial for the modal and send to the builder

function getModal($location, $options) {

    $.ajax({
        type: 'GET',
        url: $location,
        dataType: 'html',
        success: function ($modalCode) {
            buildModal($modalCode, $options);
        },
    });

}

// build then modal in preparation to send it

function buildModal($modalCode, $options) {

    var option = $.extend({

        title:         '',
        body:          '',
        footerButtons: '<span class="button redBg iconCancel closeModal">Close</span>',
        modClasses:    '',
        refresh:       'false',

    }, $options);

    var coreModal = '.modalContent';
    var modalHeader = '.modalHeader h2';
    var modalBody = '.modalBody';
    var modalFooter = '.modalButtons';

    var $modal = $($modalCode);

    // make title or remove

    if (option.title !== '') {
        $modal.find(modalHeader).append(option.title);
    } else {
        $modal.find(modalHeader).remove();
    }

    // make body or remove

    if (option.body !== '') {
        $modal.find(modalBody).append(option.body);
    }

    // make footer buttons or remove

    if (option.footerButtons !== '') {
        $modal.find(modalFooter).append(option.footerButtons);
    } else {
        $modal.find(modalFooter).remove();
    }

    // make CSS classes

    if (option.modClasses !== '') {
        $modal.find(coreModal).addClass(option.modClasses);
    }

    if (option.footerButtons !== '') {

        for (var key in option.footerButtons) {

            var emptyButton = '<a />';
            var $emptyButtonTarget = $(emptyButton);
            var keyValue = option.footerButtons[key];

            $emptyButtonTarget.html(keyValue.text).addClass(keyValue.class).attr('data-' + keyValue.dataName, keyValue.data).attr('href', keyValue.href);

            $emptyButtonTarget.click(keyValue.click);

            $modal.find(modalFooter).append($emptyButtonTarget);
        }

    }

    // sets close button to refresh

    if (option.refresh === true) {

        $(document).on('click', '.closeModal', function (e) {
            e.preventDefault();
            window.location.reload();
        });

    }

    // send modal

    returnModal($modal);

}

// send modal to page + animate

function returnModal($finalModal) {

    $('.modalTarget').append($finalModal);

    window.setTimeout(function () {

        $('.modalOverLay, .modalContent').addClass('modalOpen');

        $('.modalContainer .validateInput').each(function () {

            var $this = $(this);

            onTypingInput($this);

            formValidate($this);

            removeDefaultFormValidation();

        });

    }, 100);

}

// remove modal from DOM

function removeModal() {

    $('.modalContent').removeClass('modalOpen');

    window.setTimeout(function () {

        $('.modalOverLay').removeClass('modalOpen');

        $('.modalOverLay').one('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function () {

            $('.modalTarget').html('');

        });

    }, 500);

}

$(document).on('click', '.closeModal', function () {

    removeModal();

});
